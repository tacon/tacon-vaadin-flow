package dev.tacon.vaadin.flow.component.fluent;

@SuppressWarnings("unchecked")
public interface FluentHasAutofocus<I extends FluentHasAutofocus<I>>
		extends FluentHasElement<I> {

	void setAutofocus(boolean autofocus);

	default I withAutofocus(final boolean autofocus) {
		this.setAutofocus(autofocus);
		return (I) this;
	}
}
