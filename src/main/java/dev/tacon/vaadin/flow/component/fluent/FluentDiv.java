package dev.tacon.vaadin.flow.component.fluent;

import com.vaadin.flow.component.html.Div;

public interface FluentDiv<I extends Div & FluentDiv<I>>
		extends
		FluentComponent<I>,
		FluentHasComponents<I>,
		FluentHasSize<I>,
		FluentHasStyle<I>,
		FluentHasText<I>,
		FluentThemableLayout<I> {}
