package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;

import dev.tacon.vaadin.flow.component.fluent.FluentIcon;

public class AZIcon
		extends Icon
		implements
		FluentIcon<AZIcon> {

	private static final long serialVersionUID = -6437230583811266238L;

	public AZIcon() {
		super();
	}

	public AZIcon(final String collection, final String icon) {
		super(collection, icon);
	}

	public AZIcon(final String icon) {
		super(icon);
	}

	public AZIcon(final VaadinIcon icon) {
		super(icon);
	}
}
