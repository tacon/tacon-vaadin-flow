package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.details.Details;

import dev.tacon.vaadin.flow.component.fluent.FluentDetails;

public class AZDetails extends Details implements FluentDetails<AZDetails> {

	private static final long serialVersionUID = 6750194767136511423L;

	public AZDetails() {
		super();
	}

	public AZDetails(final Component summary, final Component content) {
		super(summary, content);
	}

	public AZDetails(final String summary, final Component content) {
		super(summary, content);
	}
}
