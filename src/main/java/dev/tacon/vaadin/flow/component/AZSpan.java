package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;

import dev.tacon.vaadin.flow.component.fluent.FluentSpan;

public class AZSpan extends Span implements FluentSpan<AZSpan> {

	private static final long serialVersionUID = -913425487813802770L;

	public AZSpan() {
		super();
	}

	public AZSpan(final Component... components) {
		super(components);
	}

	public AZSpan(final String text) {
		super(text);
	}
}
