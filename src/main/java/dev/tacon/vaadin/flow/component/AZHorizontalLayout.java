package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import dev.tacon.vaadin.flow.component.fluent.FluentHorizontalLayout;

public class AZHorizontalLayout extends HorizontalLayout implements FluentHorizontalLayout<AZHorizontalLayout> {

	private static final long serialVersionUID = -61096511349878352L;

	public AZHorizontalLayout() {
		super();
	}

	public AZHorizontalLayout(final Component... children) {
		super(children);
	}
}
