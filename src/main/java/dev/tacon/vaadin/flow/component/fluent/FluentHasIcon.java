package dev.tacon.vaadin.flow.component.fluent;

import com.vaadin.flow.component.Component;

@SuppressWarnings("unchecked")
public interface FluentHasIcon<I extends FluentHasIcon<I>>
		extends FluentHasElement<I> {

	void setIcon(Component icon);

	default I withIcon(final Component icon) {
		this.setIcon(icon);
		return (I) this;
	}
}
