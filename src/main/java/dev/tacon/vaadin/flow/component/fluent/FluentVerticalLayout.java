package dev.tacon.vaadin.flow.component.fluent;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public interface FluentVerticalLayout<I extends VerticalLayout & FluentVerticalLayout<I>>
		extends
		FluentFlexComponent<I>,
		FluentHasComponents<I>,
		FluentThemableLayout<I> {}
