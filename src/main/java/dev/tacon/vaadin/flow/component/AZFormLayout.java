package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Span;

import dev.tacon.vaadin.flow.component.fluent.FluentFormLayout;

public class AZFormLayout extends FormLayout implements FluentFormLayout<AZFormLayout> {

	private static final long serialVersionUID = -8999852588448238759L;

	public AZFormLayout() {
		super();
	}

	public AZFormLayout(final Component... components) {
		super(components);
	}

	public FormItem addFormItem(final Component component, final String label, final int colspan) {
		final FormItem formItem = this.addFormItem(component, label);
		this.setColspan(formItem, colspan);
		return formItem;
	}

	public static class AZFormItem extends FormItem implements FluentFormItem<AZFormItem> {

		private static final long serialVersionUID = 2896647172631744205L;

		public AZFormItem() {
			super();
		}

		public AZFormItem(final Component... components) {
			super(components);
		}

		public AZFormItem(final Component component, final String label, final int colspan) {
			super(component);
			this.addToLabel(new Span(label));
			this.getElement().setAttribute("colspan", colspan < 1 ? "1" : String.valueOf(colspan));
		}

		public AZFormItem withLabelWidth(final String width) {
			this.getStyle().set("--vaadin-form-item-label-width", width);
			return this;
		}

		public AZFormItem withColspan(final int colspan) {
			this.getElement().setAttribute("colspan", colspan < 1 ? "1" : String.valueOf(colspan));
			return this;
		}

		public AZFormItem withLabel(final Component component) {
			this.addToLabel(component);
			return this;
		}

		public AZFormItem withLabel(final String text) {
			return this.withLabel(new Span(text));
		}
	}
}
