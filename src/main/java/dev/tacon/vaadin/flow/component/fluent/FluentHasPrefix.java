package dev.tacon.vaadin.flow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasPrefix;

@SuppressWarnings("unchecked")
public interface FluentHasPrefix<I extends FluentHasPrefix<I>>
		extends HasPrefix, FluentHasElement<I> {

	default I withPrefixComponent(final Component component) {
		this.setPrefixComponent(component);
		return (I) this;
	}
}
