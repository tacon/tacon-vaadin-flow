package dev.tacon.vaadin.flow.component.fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.shared.HasSuffix;

@SuppressWarnings("unchecked")
public interface FluentHasSuffix<I extends FluentHasSuffix<I>>
		extends HasSuffix, FluentHasElement<I> {

	default I withSuffixComponent(final Component component) {
		this.setSuffixComponent(component);
		return (I) this;
	}
}
