package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;

import dev.tacon.vaadin.flow.component.fluent.FluentButton;

public class AZButton extends Button implements FluentButton<AZButton> {

	private static final long serialVersionUID = -1197330403991042060L;

	public AZButton() {
		super();
	}

	public AZButton(final Component icon, final ComponentEventListener<ClickEvent<Button>> clickListener) {
		super(icon, clickListener);
	}

	public AZButton(final Component icon) {
		super(icon);
	}

	public AZButton(final String text, final Component icon, final ComponentEventListener<ClickEvent<Button>> clickListener) {
		super(text, icon, clickListener);
	}

	public AZButton(final String text, final Component icon) {
		super(text, icon);
	}

	public AZButton(final String text, final ComponentEventListener<ClickEvent<Button>> clickListener) {
		super(text, clickListener);
	}

	public AZButton(final String text) {
		super(text);
	}
}
