package dev.tacon.vaadin.flow.component.fluent;

import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.HasAutocomplete;

@SuppressWarnings("unchecked")
public interface FluentHasAutocomplete<I extends FluentHasAutocomplete<I>>
		extends HasAutocomplete, FluentHasElement<I> {

	default I withAutocomplete(final Autocomplete autocomplete) {
		this.setAutocomplete(autocomplete);
		return (I) this;
	}
}
