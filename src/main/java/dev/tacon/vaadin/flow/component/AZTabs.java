package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

import dev.tacon.vaadin.flow.component.fluent.FluentTabs;

@SuppressWarnings("serial")
public class AZTabs extends Tabs implements FluentTabs<AZTabs> {

	public AZTabs() {
		super();
	}

	public AZTabs(final boolean autoselect, final Tab... tabs) {
		super(autoselect, tabs);
	}

	public AZTabs(final Tab... tabs) {
		super(tabs);
	}
}
