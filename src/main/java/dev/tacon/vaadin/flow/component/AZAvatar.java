package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.avatar.Avatar;

import dev.tacon.vaadin.flow.component.fluent.FluentAvatar;

@SuppressWarnings("serial")
public class AZAvatar extends Avatar implements FluentAvatar<AZAvatar> {

	public AZAvatar() {
		super();
	}

	public AZAvatar(final String name, final String url) {
		super(name, url);
	}

	public AZAvatar(final String name) {
		super(name);
	}
}
