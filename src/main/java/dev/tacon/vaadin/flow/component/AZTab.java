package dev.tacon.vaadin.flow.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tab;

import dev.tacon.vaadin.flow.component.fluent.FluentTab;

@SuppressWarnings("serial")
public class AZTab extends Tab implements FluentTab<AZTab> {

	public AZTab() {
		super();
	}

	public AZTab(final Component... components) {
		super(components);
	}

	public AZTab(final String label) {
		super(label);
	}
}
